<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','On');
require 'PQMVCRM.php';
?>
<!DOCTYPE html>
<html>
  <head>
    <title>PQM Semesteraufgabe</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
	<?php
		$sid = 0;
		$uid = 0;
		// Restore variables
		if (isset($_SESSION['vcrm_sid']))
			$sid = $_SESSION['vcrm_sid'];
		if (isset($_SESSION['vcrm_uid']))
			$uid = $_SESSION['vcrm_uid'];
		$pqmvcrm = new PQM_VCRM_API($sid, $uid);
		$token = $pqmvcrm->getChallenge();
		// Quick login script
		if (isset($_POST['login'])) {
			$key = $_POST['key'];
			$pqmvcrm->login($token, $key);
			$_SESSION['vcrm_sid'] = $pqmvcrm->getSessionID();
			$_SESSION['vcrm_uid'] = $pqmvcrm->getUserID();
		}
		// Quick registation script
		if (isset($_POST['register'])) {
			$username = $_POST['userName'];
			$firstname = $_POST['firstName'];
			$lastname = $_POST['lastName'];
			$email = $_POST['email'];
			$account = $pqmvcrm->createAccount($username);
			$contact = $pqmvcrm->createContact($account["id"], $firstname, $lastname, $email);
		}
		// Quick deletion script
		if (isset($_GET['delete'])) {
			$pqmvcrm->deleteObject($_GET['delete']);
		}
	?>
	<div class="container">
		<div>
			<button id="showHideChallenge" class="btn btn-success">Toogle Explanation</button>
		</div>
		<div id="challengeDiv" class="row-fluid bigdiv">
			<div class="span8">
				<p class="lead">
					Every login requires a challenge token.
				</p>
			</div>
			<div class="span4">
				<p>
					The challenge token is: <code><?=$token?></code>.
				</p>
			</div>
		</div>
	
		<div id="fullbleedcarrier" class="row-fluid bigdiv">
			<div id="formcarrier" class="span4">
				<form action="" method="post">
				  <fieldset>
					<legend>Log in</legend>
					<input type="text" placeholder="Site Key" name="key">
					<button type="submit" class="btn" name="login">Submit</button>
				  </fieldset>
				</form>
			</div>
			<div class="span1"></div>
			<div id="siteTitleDiv" class="span7">
				<h1>Selling food? Sign up today to help with your leftovers!</h1>
			</div>
			<div id="siteControlDiv" class="span7">
				<div>
					<p class="lead">
						The superior session.
					</p>
					<p>
						Well, basically this function is not necessary. But if you wanted 
						you could take over the entire site's functionality using <code>iK3N9v2RT9kLeYpC</code>
						Once you are logged in you get a session id. Normally you would take
						the admin account and hard code it.
						<? if ($pqmvcrm->getSessionID()) echo 'Session: <code>'.$pqmvcrm->getSessionID().'</code>.'; ?>
					</p>
				</div>
			</div>
		</div>
		<div class="row-fluid bigdiv" id="signupcarrier">
			<div class="span5">
				<p class="lead">
					You can sign up to our service using the form on the right side.
				</p>
				<p>
					If you like the idea of selling your leftovers for a fraction of
					the price instead of throwing them away sign up with our service!
				</p>
				<p>
					It's free and with your contribution you are helping to do a good
					thing for others who are in need for food.
				</p>
				<p>
					If you like the idea of selling your leftovers for a fraction of
					the price instead of throwing them away sign up with our service!
				</p>
			</div>
			<div class="span4">
				<p class="lead">
					Lots of people are already using it!
				</p>
				<img src="img/map.png" alt="map" />
			</div>
			<div class="span3">
				<form action="" method="post">
				  <fieldset>
					<legend>Register</legend>
					<label>Username:</label>
					<input type="text" placeholder="Username" name="userName">
					<label>First Name:</label>
					<input type="text" placeholder="Your First Name" name="firstName">
					<label>Last Name:</label>
					<input type="text" placeholder="Your Last Name" name="lastName">
					<label>eMail:</label>
					<input type="text" placeholder="eMail Address" name="email">
					<br/>
					<button type="submit" class="btn" name="register">Submit</button>
				  </fieldset>
				</form>
			</div>
		</div>
		<?php 
			if (isset($_SESSION['vcrm_sid'])) {
				$contacts = $pqmvcrm->listContacts();
			}
		?> 
		<div class="row-fluid bigdiv" id="contactsDiv">
			<table class="table table-striped">
				<tr>
					<th>Contact No</th>
					<th>Salutation</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>eMail</th>
					<th>Lead</th>
					<th>Delete</th>
				</tr>
				<? 
				if (isset($contacts)) {
					foreach ($contacts as $i => $contact) {
						echo "
						<tr>
							<td>".$contact["contact_no"]."</td>
							<td>".$contact["salutationtype"]."</td>
							<td>".$contact["firstname"]."</td>
							<td>".$contact["lastname"]."</td>
							<td>".$contact["email"]."</td>
							<td>".$contact["leadsource"]."</td>
							<td><a href='index.php?delete=".$contact['id']."'>Delete</a></td>
						</tr>
						";
					}
				}
				?>
			</table>
		</div>
	<?php
		//$pqmvcrm->listTypes($sid);
		//$accs = $pqmvcrm->describeModule($pqmvcrm->getSessionID(), 'Accounts');
		//$contact = $pqmvcrm->updateContact($sid, $contact, 'Danilo Peter', 'Schmidt');
		//$pqmvcrm->deleteObject($sid, $contact["id"]);
	?>
	</div>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="js/bootstrap.min.js"></script>
	
	<script type="text/javascript">
 
	$(document).ready(function(){
	 
		$('#showHideChallenge').click(function(){
			$('#challengeDiv').slideToggle();
			$('#siteControlDiv').slideToggle();
			$('#siteTitleDiv').slideToggle();
		});
	 
	});
	 
	</script>

  </body>
</html>

</html>