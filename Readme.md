# vTiger CRM API Examples

## Introduction

As vTiger CRM's API documentation has some severe problems this project was made publicly available as a showcase on how to use the API. The original version of vTiger's documentation can be seen [here](https://wiki.vtiger.com/index.php/Webservices_tutorials) 

## Contents

This demo includes the following aspects

* Fetching a challenge token
* Logging in
* Fetching a list of all possible modules
* Getting a module description
* Creating a contact
* Updating a contact
* Deleting a contact
* Searching a contact

## Preparations

One of the main problem of vTiger's API documentation is that they are using a deprecated version of the HTTP Client. In order to install the right one you have to fire up pear with the following install command:

    pear install HTTP_Request2

If you are experiencing any errors (or you are using a XAMPP configuration) I suggest reading this wonderful post by Dave Franklin: [http://david-franklin.net/programming/installing-pear-phpunit-on-windows/](http://david-franklin.net/programming/installing-pear-phpunit-on-windows/)

## Structure

All relevant functions are encapsulated in the PQMVCRM.php class file. You might need to modify the settings on the upper part of the document. The index.php serves as an example for a view. You can toogle hints using the button on the top. It also supports some simplistic  register methods.