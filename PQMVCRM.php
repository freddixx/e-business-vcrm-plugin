<?php
require_once 'HTTP/Request2.php';
require_once 'Zend/Json.php';

/** 
 *  This class shows some vTiger CRM API functionality that includes
 *  logging in, sessions, creating, updating and deleting contacts /
 *  accounts. The code was written according to the official tutorial
 *  @see https://wiki.vtiger.com/index.php/Webservices_tutorials
 *  However some parts were written pretty bad and the HTTP_CLIENT was
 *  deprecated. This version should work properly on PHP 5.4.X
 *  @author Friedrich Politz, Danilo Schmidt
 */
class PQM_VCRM_API {
	
	/** The URL to the web service */
	private static $SERVICE_URL 	= "http://localhost:8888/webservice.php";
	/** The default user */
	private static $USER			= "admin";
	/** The default access key */
	private static $ACCESS_KEY		= "iK3N9v2RT9kLeYpC";
	
	/** The current session id */
	private $sid;
	/** the current user id */
	private $userId;
	
	
	/**
	 *  Basic constructor.
	 */
	public function __construct($sid, $uid) {
		$this->sid = $sid;
		$this->userId = $uid; 
	}
	
	/**
	 *  This method prepares a logon process by fetching
	 *  a challenge token from the service.
	 *  @returns The challenge token
	 */
	public function getChallenge () {
		//getchallenge request must be a GET request.
		$requrl = '?operation=getchallenge%20&username='.self::$USER;
		//decode the json encode response from the server.
		$response = $this->getHTTP(self::$SERVICE_URL, $requrl);
		$jsonResponse = Zend_JSON::decode($response);
		//check for whether the requested operation was successful or not.
		if (!$this->checkJSONRequest($jsonResponse))
			die('Request failed');
		//operation was successful get the token from the reponse.
		$challengeToken = $jsonResponse['result']['token'];
		return $challengeToken;
	}
	
	/**
	 *  This method will log you in. You need to provice
	 *  the token acquired from @see getChallenge().
	 *	@param sid The challenge token
	 *  @returns The session id
	 */
	public function login($token, $key) {
		//create md5 string concatenating user accesskey from my preference page 
		//and the challenge token obtained from get challenge result. 
		$generatedKey = md5($token.$key);
		$post = array (
			'operation'	=>	'login', 
			'username'	=> 	self::$USER, 
			'accessKey'	=>	$generatedKey
		);
		//login request must be POST request.
		$response = $this->postHTTP(self::$SERVICE_URL, $post);
		//decode the json encode response from the server.
		$jsonResponse = Zend_JSON::decode($response);
		//check for whether the requested operation was successful or not.
		if (!$this->checkJSONRequest($jsonResponse))
			die('Request failed');
		//login successful extract sessionId and userId from LoginResult to it can used for further calls.
		$this->sid = $jsonResponse['result']['sessionName']; 
		$this->userId = $jsonResponse['result']['userId'];
		return $this->sid;
	}
	
	/**
	 *  This method returns all possible types.
	 *	@param sid The session id
	 *  @returns All types in an array.
	 */
	public function listTypes() {
		$get = "?sessionName=".$this->sid."&operation=listtypes";
		$response = $this->getHTTP(self::$SERVICE_URL, $get);
		//decode the json encode response from the server.
		$jsonResponse = Zend_JSON::decode($response);
		//check for whether the requested operation was successful or not.
		if (!$this->checkJSONRequest($jsonResponse))
			die('Request failed');
		//Get the List of all the modules accessible.
		$modules = $jsonResponse['result']['types'];
		return $modules;
	}
	
	/**
	 *  This method returns the full description for a
	 *  given type (module).
	 *	@param sid The module / type name
	 *  @returns The description as an array
	 */
	public function describeModule($moduleName) {
		//use sessionId created at the time of login.
		$params = "?sessionName=".$this->sid."&operation=describe&elementType=".$moduleName;
		//describe request must be GET request.
		$response = $this->getHTTP(self::$SERVICE_URL, $params);
		$jsonResponse = Zend_JSON::decode($response);
		//check for whether the requested operation was successful or not.
		if (!$this->checkJSONRequest($jsonResponse))
			die('Request failed');
		//get describe result object.
		$description = $jsonResponse['result'];
		return $description;
	}
	
	/**
	 *  This method creates a new account
	 *	@param sid The session id
	 *  @returns The new account
	 */
	public function createAccount($accountName) {
		//Create a Contact and associate with a new Account.
		//fill in the details of the Accounts.userId is obtained from loginResult.
		$accountData  = array(
			'accountname' => $accountName, 
			'assigned_user_id' => $this->userId
		);
		//encode the object in JSON format to communicate with the server.
		$objectJson = Zend_JSON::encode($accountData);
		//sessionId is obtained from loginResult.
		$params = array(
			"sessionName" => $this->sid, 
			"operation" => 'create', 
			"element" => $objectJson, 
			"elementType" => 'Accounts'
		);
		$response = $this->postHTTP(self::$SERVICE_URL, $params);
		$jsonResponse = Zend_JSON::decode($response);
		//check for whether the requested operation was successful or not.
		if (!$this->checkJSONRequest($jsonResponse))
			die('Request failed');
		$account = $jsonResponse['result']; 
		return $account;
	}
	
	/**
	 *  This method creates a contact that is associated to a
	 *  previously created account. For now only first name and
	 *  last name are supported. 
	 *	@param sid 			The session id
	 *	@param acountId 	The account id the contact is associated with
	 *	@param firstName	The first name 
	 *	@param lastName		The last name
	 *  @returns The new contact
	 */
	public function createContact($accountId, $firstName, $lastName, $email) {
		$contactData = array(
			'firstname' => $firstName,
			'lastname' => $lastName, 
			'email' => $email,
			'assigned_user_id'=>$this->userId,
			'account_id'=>$accountId
		);
		//encode the object in JSON format to communicate with the server. 
		$objectJson = Zend_JSON::encode($contactData); 
		//name of the module for which the entry has to be created. 
		$moduleName = 'Contacts';
		//sessionId is obtained from loginResult.
		$params = array(
			"sessionName"=>$this->sid, 
			"operation"=>'create',
			"element"=>$objectJson, 
			"elementType"=>$moduleName
		);
		//Create must be POST Request. 
		$response = $this->postHTTP(self::$SERVICE_URL, $params);
		//decode the json encode response from the server. 
		$jsonResponse = Zend_JSON::decode($response);
		//check for whether the requested operation was successful or not.
		if (!$this->checkJSONRequest($jsonResponse))
			die('Request failed');
		$savedObject = $jsonResponse['result']; 
		return $savedObject;
	}
	
	/**
	 *  This method updates a contact. For now only first name and
	 *  last name are supported. 
	 *	@param sid 			The session id
	 *	@param contact 		The contact
	 *	@param firstName	The first name 
	 *	@param lastName		The last name
	 *  @returns The updated contact
	 */
	public function updateContact ($contact, $firstName, $lastName) {
		$contact['firstname'] = $firstName;
		$contact['lastname'] = $lastName;
		//encode the object in JSON format to communicate with the server.
		$objectJson = Zend_JSON::encode($contact);
		$params = array(
			"sessionName"=>$this->sid, 
			"operation"=>'update', 
			"element"=>$objectJson
		);
		//Post the update to the server
		$response = $this->postHTTP(self::$SERVICE_URL, $params);
		//decode the json encode response from the server.
		$jsonResponse = Zend_JSON::decode($response);
		//check for whether the requested operation was successful or not.
		if (!$this->checkJSONRequest($jsonResponse))
			die('Request failed');
		//update result object.
		$updatedObject = $jsonResponse['result'];
		return $updatedObject;
	}
	
	/**
	 *  This method can delete ANY object in vTiger CRM and should
	 *  be used wisely.
	 *	@param sid 		The session id
	 *	@param cid 		The object id
	 *  @returns True if the request was successful
	 */
	public function deleteObject($cid) {
		//delete a record created in above examples, sessionId a obtain from the login result.
		$params = array(
			"sessionName" => $this->sid, 
			"operation" => 'delete', 
			"id" => $cid
		);
		//post the deletion request 
		$response = $this->postHTTP(self::$SERVICE_URL, $params);
		//decode the json encode response from the server.
		$jsonResponse = Zend_JSON::decode($response);
		//check for whether the requested operation was successful or not.
		if (!$this->checkJSONRequest($jsonResponse))
			die('Request failed');
		return true;
	}
	
	/**
	 *  This method returns a list of contacts or performs a specific
	 *  search for a given ID.
	 *  be used wisely.
	 *	@param id 	The contact id
	 *  @returns A list of contacts.
	 */
	public function listContacts($id = -1) {
		$query = "";
		if ($id == -1 )
			$query = "select * from Contacts;";
		else
			$query = "select * from Contacts where id=".$id.";";
		//urlencode to as its sent over http.
		$queryParam = urlencode($query);
		//sessionId is obtained from login result.
		$params = "?sessionName=".$this->sid."&operation=query&query=".$queryParam;
		//query must be GET Request.
		$response = $this->getHTTP(self::$SERVICE_URL, $params);
		//decode the json encode response from the server.
		$jsonResponse = Zend_JSON::decode($response);
		//check for whether the requested operation was successful or not.
		if (!$this->checkJSONRequest($jsonResponse))
			die('Request failed');
		//Array of vtigerObjects
		return $retrievedObjects = $jsonResponse['result'];
	}
	
	/**
	 *  getHTTP is a basic method that encapsulates a GET HTTP request.
	 *	If the request succeeds the response object is returned. If not
	 *  a status is printed.
	 *	@param url			The url for the request
	 *  @param poststring	The get data
	 *  @returns The response
	 */
	private function getHTTP ($url, $poststring) {
		$request = new HTTP_Request2($url.$poststring, HTTP_Request2::METHOD_GET);
		try {
			$response = $request->send();
			if (200 == $response->getStatus()) {
				return $response->getBody();
			} else {
				echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .$response->getReasonPhrase();
			}
		} catch (HTTP_Request2_Exception $e) {
			echo 'Error: ' . $e->getMessage();
		}
	}
	
	/**
	 *  postHTTP is a basic method that encapsulates a POST HTTP request.
	 *	If the request succeeds the response object is returned. If not
	 *  a status is printed.
	 *	@param url	The url for the request
	 *  @param post	The post data
	 *  @returns The response
	 */
	private function postHTTP ($url, $post) {
		$request = new HTTP_Request2($url);
		$request->setMethod(HTTP_Request2::METHOD_POST)
			->addPostParameter($post);
		try {
			$response = $request->send();
			if (200 == $response->getStatus()) {
				return $response->getBody();
			} else {
				echo 'Unexpected HTTP status: ' . $response->getStatus() . ' ' .$response->getReasonPhrase();
			}
		} catch (HTTP_Request2_Exception $e) {
			echo 'Error: ' . $e->getMessage();
		}
	}
	
	/**
	 *  Gets the session ID.
	 *  @returns The session ID
	 */
	public function getSessionID () {
		return $this->sid;
	}
	
	/**
	 *  Gets the user ID.
	 *  @returns The user ID
	 */
	public function getUserID () {
		return $this->userId;
	}
	
	/**
	 *  This method checks a JSON response for status errors. It's a 
	 *  little helper method to quickly analyse the success for a request.
	 *	@param jsonResponse	The JSON object
	 *  @returns True if the request was successful
	 */
	private function checkJSONRequest($jsonResponse) {
		if($jsonResponse['success']==false) {
			//handle the failure case.
			var_dump($jsonResponse);
			return false;
		}
		return true;
	}
}
?>